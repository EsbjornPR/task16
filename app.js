const consumer = {
    balance: 1000,
    loan: 0,
}

// HTML-elements
const elBalanceDisplay = document.getElementById('balance');
const elSelectedComputer = document.getElementById('computer-select');
const elBuyMessage = document.getElementById('buy-alert');
const elBannedMessage = document.getElementById('banned-alert');
const elWorkBtn = document.getElementById('work-btn');
const elLoanBtn = document.getElementById('get-loan-btn');
const elBuyBtn = document.getElementById('buy-btn');

// Initial GUI state
elBuyMessage.hidden = true;
elBannedMessage.hidden = true;
elBalanceDisplay.innerText = `Bank balance: ${consumer.balance} kr.`;

// EventListener for the work-button
elWorkBtn.addEventListener('click', function () {
    consumer.balance = consumer.balance + 10;
    elBalanceDisplay.innerText = `Bank balance: ${consumer.balance} kr.`;
    console.log(consumer.balance);
})

// EventListener for the loan-button
elLoanBtn.addEventListener('click', function () {
    consumer.loan = (elSelectedComputer.value - consumer.balance) * 0.9;
    consumer.balance = consumer.balance + consumer.loan;
    elBalanceDisplay.innerText = `Bank balance: ${consumer.balance} kr.`;
    console.log(consumer.balance);
    elLoanBtn.disabled = true;
})

// EventListener for the buy-button
elBuyBtn.addEventListener('click', function () {
    if (consumer.balance < elSelectedComputer.value) {
        console.log('You´re a loser')
        elBuyBtn.disabled = true;
        elBannedMessage.hidden = false;
    } else {
        consumer.balance = consumer.balance - elSelectedComputer.value;
        elBalanceDisplay.innerText = `Bank balance: ${consumer.balance} kr.`;
        elBuyMessage.hidden = false;
        console.log('You got yourself a computer')
    }
})